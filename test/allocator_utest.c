#include "allocator.h"
#include "mu_test.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

void printBuffer(const char* message, char* _buff, size_t _size) {
#ifdef DEBUG
	size_t i = 0;
	printf("%s", message);
	for (; i < _size ; ++i) {
		printf("0x%x ", _buff[i] & 0xff);
	}
	printf("\n");
#endif
}

void fillBuffer(uint8_t* _buff, size_t _size) {
	size_t i = 0;
	for (; i < _size ; ++i) {
		_buff[i] = 0xff;
	}
}
#define T001_TOTAL_SIZE (100)
UNIT(t001_allocate_init_destroy)
	size_t totalSize = T001_TOTAL_SIZE;
	char myMemory[T001_TOTAL_SIZE] = {0};
	char* verifyPtr = &myMemory[0];
	char* destroyPtr = NULL;

	Allocator* alloc = AllocatorInit(totalSize, myMemory);
	ASSERT_THAT(alloc != NULL);
	destroyPtr = AllocatorDestroy(alloc);
	ASSERT_THAT(destroyPtr != NULL);
	ASSERT_THAT(destroyPtr == verifyPtr);
END_UNIT

UNIT(t002_allocate_init_get_one_memory)
	size_t totalSize = T001_TOTAL_SIZE;
	char myMemory[T001_TOTAL_SIZE] = {0};
	char* destroyPtr = NULL;
	void* allocated = NULL;

	Allocator* alloc = AllocatorInit(totalSize,myMemory);
	ASSERT_THAT(alloc != NULL);
	allocated = AllocatorAllocateMemory(alloc, 3);
	ASSERT_THAT(allocated != NULL);
	fillBuffer(allocated, 3);	
	destroyPtr = AllocatorDestroy(alloc);
	ASSERT_THAT(destroyPtr != NULL);
END_UNIT

UNIT(t003_allocate_init_no_free_memory)
	size_t totalSize = T001_TOTAL_SIZE;
	char myMemory[T001_TOTAL_SIZE] = {0};
	char* destroyPtr = NULL;
	void* allocated = NULL;

	Allocator* alloc = AllocatorInit(totalSize,myMemory);
	ASSERT_THAT(alloc != NULL);
	allocated = AllocatorAllocateMemory(alloc, 100);
	ASSERT_THAT(allocated == NULL);
	
	destroyPtr = AllocatorDestroy(alloc);
	ASSERT_THAT(destroyPtr != NULL);
END_UNIT

UNIT(t004_allocate_init_get_one_memory_free_memory)
	size_t totalSize = T001_TOTAL_SIZE;
	char myMemory[T001_TOTAL_SIZE] = {0};
	char* destroyPtr = NULL;
	void* allocated = NULL;

	Allocator* alloc = AllocatorInit(totalSize,myMemory);
	ASSERT_THAT(alloc != NULL);
	printBuffer("\n\n New init allocated memory\n", myMemory, T001_TOTAL_SIZE);
	allocated = AllocatorAllocateMemory(alloc, 3);
	ASSERT_THAT(allocated != NULL);
	fillBuffer(allocated, 3);
	printBuffer("\n\n 3 bytes allocated\n", myMemory, T001_TOTAL_SIZE);

	AllocatorFreeMemory(alloc, allocated);
	printBuffer("\n\n Free memory\n", myMemory, T001_TOTAL_SIZE);
	destroyPtr = AllocatorDestroy(alloc);
	ASSERT_THAT(destroyPtr != NULL);
END_UNIT

#define T005_MEMORYSIZE (8+9)
UNIT(t005_not_enoug_memory_to_allocated)
	size_t totalSize = T005_MEMORYSIZE;
	char myMemory[T005_MEMORYSIZE] = {0};
	Allocator* alloc = AllocatorInit(totalSize, myMemory);
	ASSERT_THAT(alloc == NULL);
END_UNIT

#define T006_MEMORYSIZE (20)
UNIT(t006_allocated_memory_to_large_request)
	size_t totalSize = T006_MEMORYSIZE;
	char myMemory[T006_MEMORYSIZE] = {0};
	void* destroyPtr = NULL;
	char* allocated = NULL;
	Allocator* alloc = AllocatorInit(totalSize, myMemory);
	ASSERT_THAT(alloc != NULL);
	allocated = AllocatorAllocateMemory(alloc, 20);
	ASSERT_THAT(allocated == NULL);
	destroyPtr = AllocatorDestroy(alloc);
	ASSERT_THAT(destroyPtr != NULL);
END_UNIT

UNIT(t007_AllocatorInit_AllocatorDestroy_NULL_check)
	size_t totalSize = T006_MEMORYSIZE;
	char myMemory[T006_MEMORYSIZE] = {0};
	Allocator* alloc = AllocatorInit(totalSize, NULL);
	ASSERT_THAT(alloc == NULL);
	alloc = AllocatorInit(0, myMemory);
	ASSERT_THAT(alloc == NULL);
	alloc = AllocatorDestroy(NULL);
	ASSERT_THAT(alloc == NULL);
END_UNIT


UNIT(t008_AllocatorAllocateMemory_AllocatorFreeMemory_NULL_check)
	size_t totalSize = T001_TOTAL_SIZE;
	char myMemory[T001_TOTAL_SIZE] = {0};
	void* destroyPtr = NULL;
	char* allocated = NULL;
	Allocator* alloc = AllocatorInit(totalSize, myMemory);
	ASSERT_THAT(alloc != NULL);
	allocated = AllocatorAllocateMemory(alloc, 0);
	ASSERT_THAT(allocated == NULL);
	allocated = AllocatorAllocateMemory(NULL, 3);
	ASSERT_THAT(allocated == NULL);

	allocated = AllocatorAllocateMemory(alloc, 3);
	ASSERT_THAT(allocated != NULL);
	AllocatorFreeMemory(NULL, allocated);
	AllocatorFreeMemory(alloc, NULL);

	AllocatorFreeMemory(alloc, allocated);
	AllocatorFreeMemory(alloc, allocated);
	destroyPtr = AllocatorDestroy(alloc);
	ASSERT_THAT(destroyPtr != NULL);
END_UNIT

TEST_SUITE(Test ListItr)
	TEST(t001_allocate_init_destroy)
	TEST(t002_allocate_init_get_one_memory)
	TEST(t003_allocate_init_no_free_memory)
	TEST(t004_allocate_init_get_one_memory_free_memory)
	TEST(t005_not_enoug_memory_to_allocated)
	TEST(t006_allocated_memory_to_large_request)
	TEST(t007_AllocatorInit_AllocatorDestroy_NULL_check)
	TEST(t008_AllocatorAllocateMemory_AllocatorFreeMemory_NULL_check)
END_SUITE