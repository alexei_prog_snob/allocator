# compiler
CC := gcc

# PATH
OBJ_PATH := ./obj
INC_PATH := ./inc
SRC_PATH := ./src
TEST_PATH := ./test

INC := -I$(INC_PATH)
CFLAGS := -ansi -pedantic -Werror -Wall $(INC)
OBJ := $(OBJ_PATH)/allocator.o

.PHONY : all
.PHONY : utest
.PHONY : clean_utest
.PHONY : clean

all: clean_utest $(OBJ) utest

$(OBJ_PATH)/%.o:$(SRC_PATH)/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

utest:
	cd $(TEST_PATH) && $(MAKE)

clean_utest:
	rm -rf ./test/allocator_utest

clean :
	rm -rf *.o
	cd $(TEST_PATH) && $(MAKE) clean
	
