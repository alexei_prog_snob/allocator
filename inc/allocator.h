#ifndef __ALLOC_H__
#define __ALLOC_H__

/** 
 *  @file alloc.h
 *  @brief Create memory meneger
 * 
 *  @details Implemented memory maneger like malloc.
 * 
 *  @author Author Alexei Radashkovsky (alexeirada@gmail.com) 
 *  @Update by Alexei Radashkovsky on 16/11/16
 *  @bug Some.
 */ 

#include <stddef.h> /* size_t */

typedef struct Allocator Allocator;

/** 
 * @brief Init memory 
 * 
 * @params[in] _totalSize : memory size to meneg; 
 * @returns a pointer Allocator.
 * @retval NULL on failure due to short memory
 */
Allocator* AllocatorInit(size_t _totalSize, void* _mem);

/** 
 * @brief Destroy al memory
 * 
 * @params[in] _alloc : pointer to Allocator.
 */
void* AllocatorDestroy(Allocator* _alloc);

/** 
 * @brief Allocat memory  
 * 
 * @params[in] _alloc : pointer to Allocator.
 * @params[in] _size : size of memory to aloocat. 
 * @returns a pointer to free memory.
 * @retval NULL on failure due to short memory
 */
void* AllocatorAllocateMemory(Allocator* _alloc,size_t _size);

/** 
 * @brief Free memory
 * 
 * @params[in] _alloc : pointer to Allocator.
 * @params[in] _place : memory to free.
 */
void AllocatorFreeMemory(Allocator* _alloc,void* _place);



#endif /* _ALLOC_H__ */