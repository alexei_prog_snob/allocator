#include "allocator.h"
#include <string.h> /*< memset >*/
#include <stdint.h> /*< uint8_t >*/

#pragma pack(push, 1)
struct Allocator {
    size_t m_totalCapasity;
    uint8_t m_memory[1];
};

/****** STATIC FUNCTIONS DECLARATIONS ******/
typedef union _MemoryUnion {
	struct {
		unsigned m_free:1;
	} u_flags;
	uint8_t u_flagSize;
} MemoryUnion;

typedef struct _MemoryObject {
	size_t m_memorySize;
	MemoryUnion m_status;
	uint8_t m_memoryToUse[1];
} MemoryObject;

#define SIZEOF_ALLOCATOR (sizeof(Allocator) - sizeof(uint8_t))
#define SIZEOF_MEMORYOBJECT (sizeof(MemoryObject) - sizeof(uint8_t))
/**
 * check if MemoryObject is free.
 * 0 is free , other allocated
 * */
static int8_t IsFree(MemoryObject* _currentMemory);

/**
 * Get the next MemoryObject
 * 
*/
static MemoryObject* GetNextMemoryObject(MemoryObject* _currentMemory);

/**
 * Looking for the first free memory
 * If there are no free memory return NULL
*/
static MemoryObject* FindFirstFreeMemory(MemoryObject* _currentMemory, size_t _end);

/**
 * Calculat if there enough memory left to allocate
 * New MemoryObject with free memory buffer
 * 0 there is, other fail
*/
static int8_t IsThereEnoughMemory(MemoryObject* _currentMemory);

/**
 * Calculate the new MemoryObjectmemroy size from excisting MemoryObject
 */
static size_t NewMemoryObjectMemorySize(MemoryObject* _foundMemory,size_t _size);

/**
 * Init all data that need to for allocated new memory for user
 */
static void InitMemoryForAllocation(MemoryObject* _foundMemory, size_t _size, size_t _endPtr);

/**
 * Looking for new memory to allocate NULL if there no free memroy
*/
static void* FindFreeMemoryToUse(Allocator* _alloc, size_t _size);

/**
 * Attach Adjacent Memory to one big free memory
*/
static void AttachAdjacentMemory(MemoryObject* _currentMemory, size_t _endPtr);

/**
 * Free memory and Attach Adjacent Memory
*/
static void FreeMemory(Allocator* _alloc, uint8_t* _mem);
/**
 * Init All data to new allocator
 * return if there enough memory
 */
static int InitAllocatorNewData(Allocator* _newAllocator, size_t _totalSize);
/** 
 * @brief Init memory 
 * 
 * @params[in] _totalSize : memory size to meneg; 
 * @returns a pointer Allocator.
 * @retval NULL on failure due to short memory
 */
Allocator* AllocatorInit(size_t _totalSize,void* _mem) {
    Allocator* newMemoryMgr = NULL;
    if(_mem == NULL || _totalSize < sizeof(Allocator))  {
        return NULL;
    }
	memset(_mem, 0, _totalSize);
    newMemoryMgr = (Allocator*)_mem;
    return InitAllocatorNewData(newMemoryMgr, _totalSize) == 0 ? newMemoryMgr : NULL;
}

/** 
 * @brief Destroy al memory
 * 
 * @params[in] _alloc : pointer to Allocator.
 * @returns your allocated orginal memory.
 * @retval NULL if you pass _alloc as NULL
 */
void* AllocatorDestroy(Allocator* _alloc) {
    size_t orignalSize;
    if (_alloc == NULL) {
        return NULL;
    }

    orignalSize = _alloc->m_totalCapasity;
    memset(_alloc, 0, orignalSize);
    return _alloc;
}

void* AllocatorAllocateMemory(Allocator* _alloc, size_t _size) {
	if (_alloc == NULL || _size == 0) {
		return NULL;
	}
	
	if (_size >= _alloc->m_totalCapasity) {
		return NULL;
	}

    return FindFreeMemoryToUse(_alloc, _size);
}

void AllocatorFreeMemory(Allocator* _alloc, void* _mem) {
	if (_alloc == NULL || _mem == NULL) {
		return;
	}
	FreeMemory(_alloc, _mem);
}

/****** STATIC FUNCTIONS IMPLEMENTATION ******/
static size_t GetEndPrt(Allocator* _alloc) {
	return (size_t)_alloc + _alloc->m_totalCapasity;
}

static int8_t IsFree(MemoryObject* _currentMemory) {
	if ((_currentMemory->m_status).u_flags.m_free != 0) {
		return 0;
	}
	return -1;
}

static MemoryObject* GetNextMemoryObject(MemoryObject* _currentMemory) {
	return (MemoryObject*)(_currentMemory->m_memorySize + _currentMemory->m_memoryToUse);
}

static MemoryObject* FindFirstFreeMemory(MemoryObject* _currentMemory, size_t _end) {
	if ((size_t)_currentMemory == _end) {
		return NULL;
	}

	if (IsFree(_currentMemory) == 0) {
		return _currentMemory;
	}

	return FindFirstFreeMemory(GetNextMemoryObject(_currentMemory), _end);
}

static int8_t IsThereEnoughMemory(MemoryObject* _currentMemory) {
	if ((int64_t)(_currentMemory->m_memorySize - sizeof(MemoryObject)) > 0) {
		return 0;
	}
	return -1;
}

static size_t NewMemoryObjectMemorySize(MemoryObject* _foundMemory,size_t _size) {
	return _foundMemory->m_memorySize - _size - SIZEOF_MEMORYOBJECT;
}

static void InitMemoryForAllocation(MemoryObject* _foundMemory, size_t _size, size_t _endPtr) {
	MemoryObject* newFreeMemory = NULL;
	(_foundMemory->m_status).u_flags.m_free = 0;
	if (_foundMemory->m_memorySize != _size) {
		newFreeMemory = (MemoryObject*)(_foundMemory->m_memoryToUse + _size);
		if (((size_t)newFreeMemory + sizeof(MemoryObject)) < _endPtr ) {
			(newFreeMemory->m_status).u_flags.m_free = 1;
			newFreeMemory->m_memorySize = NewMemoryObjectMemorySize(_foundMemory, _size);
			_foundMemory->m_memorySize = _size;
		}
	}
}

static void* FindFreeMemoryToUse(Allocator* _alloc, size_t _size) {
	size_t endPtr = GetEndPrt(_alloc);
	MemoryObject* currentMemory = (MemoryObject*)_alloc->m_memory;
	MemoryObject* bestFreeMemory = FindFirstFreeMemory(currentMemory, endPtr);
	if (bestFreeMemory == NULL) {
		return NULL;
	}

	for (	currentMemory = GetNextMemoryObject(bestFreeMemory); 	
			(size_t)currentMemory < endPtr; 
			currentMemory = GetNextMemoryObject(currentMemory)
		) {
		if (IsFree(currentMemory) == 0) {
			if (currentMemory->m_memorySize == _size) {
				bestFreeMemory = currentMemory;
				break;
			}
			
			if (currentMemory->m_memorySize > _size) {
				if (currentMemory->m_memorySize > bestFreeMemory->m_memorySize) {
						bestFreeMemory = currentMemory;
				}
			}
		}
	}

	if (bestFreeMemory->m_memorySize < _size) {
		return NULL;
	}

	if (IsThereEnoughMemory(bestFreeMemory) != 0) {
		_size = bestFreeMemory->m_memorySize;
	}
	InitMemoryForAllocation(bestFreeMemory, _size, endPtr);
	return bestFreeMemory->m_memoryToUse;
}



static void AttachAdjacentMemory(MemoryObject* _currentMemory, size_t _endPtr) {
	MemoryObject* nextMemory = GetNextMemoryObject(_currentMemory);
	if ((size_t)nextMemory >= _endPtr) {
		return;
	}
	
	if (IsFree(_currentMemory) 	== 0 &&
		IsFree(nextMemory)		== 0) {
		_currentMemory->m_memorySize = _currentMemory->m_memorySize  + nextMemory->m_memorySize + SIZEOF_MEMORYOBJECT;
	}
	AttachAdjacentMemory(_currentMemory, _endPtr);
	
}

static void FreeMemory(Allocator* _alloc, uint8_t* _mem) {
	size_t endPtr = GetEndPrt(_alloc);
	MemoryObject* currentMemory = (MemoryObject*)(_mem - SIZEOF_MEMORYOBJECT);
	if ((currentMemory->m_status).u_flags.m_free == 1) {
		return;
	}
	(currentMemory->m_status).u_flags.m_free = 1;
	AttachAdjacentMemory(currentMemory, endPtr);
}

static int InitAllocatorNewData(Allocator* _newAllocator, size_t _totalSize) {
    MemoryObject* newMemory = (MemoryObject*)_newAllocator->m_memory;
	(newMemory->m_status).u_flags.m_free = 1;
	newMemory->m_memorySize = _totalSize - SIZEOF_ALLOCATOR - SIZEOF_MEMORYOBJECT;
	_newAllocator->m_totalCapasity = _totalSize;
	if (newMemory->m_memorySize == 0) {
		return -1;
	}
	return 0;
}